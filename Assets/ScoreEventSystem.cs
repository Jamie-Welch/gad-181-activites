using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScoreEventSystem
{
    public delegate void FloatDelegate(float value);
    public static FloatDelegate AddScoreEvent;
}