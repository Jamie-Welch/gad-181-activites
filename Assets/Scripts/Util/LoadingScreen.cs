using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAE;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    public float timeCount;
    public float ttw;
    public CommonActions coomonScript;

    public bool buttonTime;
    public GameObject buttonObject;
    public RawImage videoImage;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;
        if (timeCount >= ttw)
        {
            buttonTime = true;
            if(buttonTime == true)
            {
                buttonObject.SetActive(true);
                videoImage.gameObject.SetActive(false);
            }
        }
    }
}
