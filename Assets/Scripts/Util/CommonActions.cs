﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Helper to be added to for actions and commands that are desired to be triggered by UnityEvents, such as
    /// button presses or inspector binding.
    /// </summary>
    public class CommonActions : MonoBehaviour
    {
        public void LevelLoad(string levelName)
        {
            LoadingScreen.LoadScene(levelName);
        }

        public void ExitGame()
        {
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
    }
}
