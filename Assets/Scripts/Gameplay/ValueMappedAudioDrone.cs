﻿using UnityEngine;

namespace SAE.Example
{
    // POD, would be a struct but Unity's c# version doesn't allow simple clean implementation here
    [System.Serializable]
    public class ValueMappedAudioDrone
    {
        public float curValue;
        public float adaptiveness = 100;
        public Vector2 valueRange = new Vector2(0, 1);
        public AnimationCurve volumeCurve = AnimationCurve.Constant(0, 1, 1);
        public AnimationCurve pitchCurve = AnimationCurve.Constant(0, 1, 1);

        public void Init(float value, AudioSource audioSource)
        {
            curValue = Mathf.Clamp(value, valueRange.x, valueRange.y);
            Tick(value, 1, audioSource);
        }

        public void Tick(float value, float dt, AudioSource audioSource)
        {
            curValue = Mathf.Clamp(Mathf.SmoothStep(curValue, value, Time.deltaTime * adaptiveness), valueRange.x, valueRange.y);
            audioSource.volume = volumeCurve.Evaluate(curValue);
            audioSource.pitch = pitchCurve.Evaluate(curValue);
        }

        public void CopyFrom(ValueMappedAudioDrone other)
        {
            adaptiveness = other.adaptiveness;
            valueRange = other.valueRange;
            volumeCurve = other.volumeCurve;
            pitchCurve = other.pitchCurve;
        }
    }
}
