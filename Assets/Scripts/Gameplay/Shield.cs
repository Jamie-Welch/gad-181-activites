using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAE;

public class Shield : Weapon
{
    [SerializeField] protected int spawnAmount;
    [SerializeField] protected float radiusSize;
    public override void Fire()
    {
        Vector2 centre = transform.position;
        for( int i = 0; i < spawnAmount; i++)
        {
            Vector3 pos = setCircle(centre, radiusSize);
            Quaternion rot = Quaternion.identity;
            Instantiate(projectilePrefab.gameObject, pos, rot);
        }
    }

    Vector2 setCircle(Vector2 centre, float radius)
    {
        float ang = Random.value * 360;
        Vector2 pos;
        pos.x = centre.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = centre.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        return pos;
    }
}
