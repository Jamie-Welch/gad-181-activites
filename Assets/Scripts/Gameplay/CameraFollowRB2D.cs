using UnityEngine;

namespace SAE.Example
{
    /// <summary>
    /// Simple offset style camera follow, to be replaced.
    /// </summary>
    public class CameraFollowRB2D : MonoBehaviour
    {
        [SerializeField] protected Transform cameraTransform;
        [SerializeField] protected Rigidbody2D targetRB2D;
        [SerializeField] protected Vector3 offset = new Vector3(0, 0, -10);
        [SerializeField] protected GameObject playerSpawn;

        private void Awake()
        {
            targetRB2D = playerSpawn.GetComponentInChildren<Rigidbody2D>();
        }

        

        void Update()
        {
            cameraTransform.position = targetRB2D.transform.position + offset;
        }



    }
}