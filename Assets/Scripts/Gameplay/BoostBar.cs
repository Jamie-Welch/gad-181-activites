using SAE;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class BoostBar : MonoBehaviour
{
    public Image boostSlider;   //we can update our boost slider
    public float boostRegenSpeed;    //how fast we regenerate our boost bar
    public float boostDrainRate;    //how fast our boost drains when used
    public float maxBoost;      //how much boost we can hold
    public float currentBoost;  //how much boost we have right now
    public bool debug;          //start debugging
    public float tooTired;      //A float that will be compared to allow boost below a certain amount
    public int fractionOfMax;   //what fraction of our max boost tooTired will be
    
    public string hullChoice;
    public string wingChoice;
    public Hull hullStandard;
    public Hull hullHeavy;
    public Hull hullLight;
    public Wing wingGlide;
    public Wing wingDash;
    public Wing wingStandard;

    private void Start()
    {
        hullChoice = PlayerPrefs.GetString("HullType");

        //The check of which hull the player game object has attached to it - put in Awake if using.
        if (hullChoice == "Heavy")
        {
            maxBoost = maxBoost * hullHeavy.hullMaxBoostMod;
            
            Debug.Log("My Hull is Heavy for max boost! and my max boost is " + maxBoost);
        }
        else if (hullChoice == "Light")
        {
            maxBoost = maxBoost * hullLight.hullMaxBoostMod;
            Debug.Log("My Hull is Light for max boost! and my max boost is " + maxBoost);
        }
        else
        {
            maxBoost = maxBoost * hullStandard.hullMaxBoostMod;
            Debug.Log("My Hull is Standard for max boost! and my max boost is " + maxBoost +"and my drain rate is " +boostDrainRate);
        }

        wingChoice = PlayerPrefs.GetString("WingType");

        //The check of which wings the player game object has attached to it.
        if (wingChoice == "Glide")
        {
            boostRegenSpeed = boostRegenSpeed * wingGlide.wingBoostRegenMod;
            fractionOfMax = wingGlide.wingBoostFractionMod;
            boostDrainRate = boostDrainRate * wingGlide.wingBoostDrainRateMod;
            Debug.Log("My wing is Glide for boost Regen speed! My Regen speed is" + boostRegenSpeed);
        }
        else if (wingChoice == "Dash")
        {
            boostRegenSpeed = boostRegenSpeed * wingDash.wingBoostRegenMod;
            fractionOfMax = wingDash.wingBoostFractionMod;
            boostDrainRate = boostDrainRate * wingDash.wingBoostDrainRateMod;
            Debug.Log("My wing is Dash for boost Regen speed! My Regen speed is" + boostRegenSpeed);
        }
        else
        {
            boostRegenSpeed = boostRegenSpeed * wingStandard.wingBoostRegenMod;
            fractionOfMax = wingStandard.wingBoostFractionMod;
            boostDrainRate = boostDrainRate * wingStandard.wingBoostDrainRateMod;
            Debug.Log("My wing is Standard for boost Regen speed! My Regen speed is" + boostRegenSpeed);
        }

        currentBoost = maxBoost;           //update the current amount of boost to whatever we're allowed to start with
        boostSlider.fillAmount = maxBoost; //update the slider to represent our current boost amount.
        tooTired = maxBoost / fractionOfMax;    //set the tooTired variable to a fraction of our max boost.
    }

    public void RegenBoost()    //Gets called in player input router Update
    {
        if (debug)  //If we have debugging enabled, send a message out saying we've called this function
        {
            Debug.Log("We're calling the RegenBoost function");
        }
        if(currentBoost <= maxBoost)
        {
            currentBoost += boostRegenSpeed * Time.deltaTime;   //increase our current boost level   
            float regenAmount = currentBoost / maxBoost; //convert to number between 0 and 1
            boostSlider.fillAmount = regenAmount;   //update the boost bar image fill to that number
            if (tooTired <= currentBoost)               //if we get high enough to boost again 
            {
                boostSlider.color = Color.yellow;       //change to yellow to show player we can boost
            }
        }
        
    }
    public void DrainBoost()    //do the same as above, but reduce instead of increase.
    {
        if (debug)
        {
            Debug.Log("We're calling the DrainBoost function");
        }
        if (currentBoost >= 1f)
        {
            currentBoost -= boostDrainRate * Time.deltaTime;
            float drainAmount = currentBoost / maxBoost;
            boostSlider.fillAmount = drainAmount;
        }
        
        if(tooTired >= currentBoost)           //if we get low enough, show player we can't boost if we let go
        {
            boostSlider.color = Color.red;  //set colour to red to show player if they stop boosting they'll need to wait for regen.
        }
    }
}


        /*//variables
        public float currentBoost;
        [SerializeField] private float totalBoost;
        public bool isMoving = false;
        public bool hasRegenerated = true;

        [Range(0, 50)][SerializeField] private float boostRegen = 0.5f;
        [Range(0, 50)][SerializeField] private float boostDrain = 0.5f;

        [SerializeField] private Image boostProgressUI = null;
        [SerializeField] private CanvasGroup boostSliderCanvasGroup = null;

        private void Start()
        {

        }

        private void Update()
        {
            if(!isMoving)
            {
                if(currentBoost <= totalBoost -0.01f)
                {
                    currentBoost += boostRegen * Time.deltaTime;
                    UpdateBoost(1);
                }
            }
        }

        void UpdateBoost(int value)
        {
            boostProgressUI.fillAmount = currentBoost / totalBoost;

            if(value == 0)
            {
                boostSliderCanvasGroup.alpha = 0;
            }
            else
            {
                boostSliderCanvasGroup.alpha = 1;
            }
        }
        */
