using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SAE;

[CreateAssetMenu(fileName = "Wing", menuName = "ScriptableObjects/Wing", order = 2)]

public class Wing : ScriptableObject
{

    //This script works just to house some variables that will be accessed from relevant scripts i.e. The player drag coeff script will take the CoeffFricMod value
    public float wingEngineForceMod;
    public float wingTurnRateMod; 
    public float wingBoostRegenMod;
    public float wingBoostDrainRateMod;
    public int wingBoostFractionMod;
}

