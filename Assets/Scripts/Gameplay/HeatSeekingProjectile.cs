﻿using UnityEngine;

namespace SAE.Example
{
    /// <summary>
    /// Extends projectile to add selection and simple rotate to face a selected transform.
    ///
    /// Uses GameObjectCollection to allow for external configuration such as only adding certain enemy
    /// types to the collection to re-enforce the 'heat seeking' nature if desired
    /// </summary>
    public class HeatSeekingProjectile : Projectile
    {
        [Tooltip("Degrees per second")]
        [SerializeField] protected float turnRate;

        [SerializeField] protected GameObjectCollectionSO targets; //this is a bit fancy, its a runtime list that objects add and remove themselves from.
        protected Transform currentTarget;

        public override void Start()
        {
            base.Start();
            SelectTarget();
        }

        protected virtual void SelectTarget()
        {
            var col = targets.Collection;
            if (col.Count > 0)
            {
                //if a potential target exists, take it
                currentTarget = col[0].transform;
                var bestDist = Vector3.Distance(transform.position, currentTarget.position);

                //check the rest of the potential targets to see if they are a better match.
                //  in this case 'better match' just means closer
                for (int i = 1; i < col.Count; i++)
                {
                    var curDist = Vector3.Distance(transform.position, col[i].transform.position);
                    if (curDist < bestDist)
                    {
                        bestDist = curDist;
                        currentTarget = col[i].transform;
                    }
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            if (currentTarget != null)
            {
                var dirToTarget = (currentTarget.position - transform.position).normalized;
                rb.velocity = Vector3.RotateTowards(rb.velocity.normalized, dirToTarget, turnRate * Time.deltaTime * Mathf.Deg2Rad, 0).normalized * speed;
                transform.rotation = Quaternion.LookRotation(Vector3.forward, rb.velocity.normalized);
            }
            //unclear if we want to find a target if one isn't set
        }
    }
}
