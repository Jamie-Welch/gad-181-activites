using SAE;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class PlayerAeroDrag : AeroDynamicDrag
{
    public Hull hullStandard;
    public Hull hullLight;
    public Hull hullHeavy;
    private string hullChoice;

    private void Awake()
    {
        hullChoice = PlayerPrefs.GetString("HullType");

        //The check of which hull the player game object has attached to it - put in Awake if using.
        if (hullChoice == "Heavy")
        {
            coeffOfFriction = hullHeavy.hullCoeffFricMod;
            Debug.Log("My Hull is Heavy for CoeffFric! and my coeff is " + coeffOfFriction);
        }
        else if (hullChoice == "Light")
        {
            coeffOfFriction = hullLight.hullCoeffFricMod;
            Debug.Log("My Hull is Light for CoeffFric and my coeff is " + coeffOfFriction);
        }
        else
        {
            coeffOfFriction = hullStandard.hullCoeffFricMod;
            Debug.Log("My Hull is Standard for CoeffFric! and my coeff is " + coeffOfFriction);
        }
    }
}
