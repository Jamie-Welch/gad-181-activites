using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundFlood : MonoBehaviour
{
    public float forceAmount;
    private Rigidbody2D playerRB;
    public void OnTriggerStay2D(Collider2D collision)
    {
        playerRB = collision.GetComponent<Rigidbody2D>();
        playerRB.AddForce(Vector2.up * forceAmount);
    }
}
