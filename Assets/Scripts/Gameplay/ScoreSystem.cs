using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SAE
{

public class ScoreSystem : MonoBehaviour
{
    //Variables
    public int currentScore;                //The value for the current score
    public int startingScore;               //Our starting score, 0
    public Text scoreText;                  //Reference the text we will be changing when updating score value
    public string scoreString = "Score: ";  //The basis for all our score text values
    public int scoreMultiplier = 1;         //The amount the score is multiplied by when an enemy is killed. Minimum value = 1
    private float multiplierTimer;          //The countdown that resets/reduces the score multiplier after a certain amount of time - set in inspector\
    public float timerTotal;                //The value we can reset our timer to when we run out or get a kill
    public int enemiesKilled;               //The ongoing number of enemies killed, once over a threshold, multiplier goes up and this is reset
    public int multiplierThreshold;         //The threshold
    public bool debug;                      //Whether we want debug on or off
    public int bossSpawnThreshold;
    public BossSpawner myBossSpawner;
    public QuantitySpawner quantitySpawnerRef;
    private void Start()
    {
        //Set the text value for our starting score
        ScoreSetup(scoreText);
        myBossSpawner = FindObjectOfType<BossSpawner>();
            quantitySpawnerRef = FindObjectOfType<QuantitySpawner>();
    }

    private void Update()
    {
        multiplierTimer -= Time.deltaTime;  //reduce time on our timer
        if (multiplierTimer <= 0)            //if it gets to 0
        {
            scoreMultiplier = 1;            //Reset the score multiplier
            TimerReset();                   //Reset the timer and score multiplier   
        }
    }
    public void ScoreSetup(Text text)
    {
        text.text = scoreString + startingScore;    //when starting, ensure all values are set correctly
    }

    //Call from enemies to update the score on enemy Death(), passing a value from the enemy
    public void AddScore(int score) //Called by enemies when they die
    {
        enemiesKilled++;                                //Increase the number of enemies killed
        if (enemiesKilled > multiplierThreshold)         //if it gets to our determined threshold to up the multiplier
            {
                scoreMultiplier++;                         //increase the multiplier
                enemiesKilled = 0;                          //reset the number of enemies killed so we can increase the multiplier further if the player kills enough in time
            }
            currentScore += score * scoreMultiplier;        //Actually add the score with updated multiplier values
            scoreText.text = scoreString + currentScore;    //Update the Text to reflect increase in score.
        
        if (currentScore >= bossSpawnThreshold)
        {
            myBossSpawner.SpawnNextBoss();
            quantitySpawnerRef.level++;
                
            bossSpawnThreshold += bossSpawnThreshold;
                Debug.Log("My Quantity Spawner Level is " + quantitySpawnerRef.level + "And my score is " + currentScore +". and my boss spawn threshold is " +bossSpawnThreshold);
            }

        if (debug)   //debug log
        {
            Debug.Log("Current Score = " + currentScore + ". Current Multiplier = " + scoreMultiplier + ". Current Enemies Killer = " + enemiesKilled + "Current Timer = " + multiplierTimer);
        }
    }
    public void TimerReset()
        {              
        multiplierTimer = timerTotal;
        }
}
}