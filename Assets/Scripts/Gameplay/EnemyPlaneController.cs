﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// The present method used here is trying to be near the player and not flying the wrong way
    /// away from them.
    ///
    /// Steve has put a lot of toggles in here so you can uncover how different parts of the
    /// logic change the resulting behaviour of the enemy planes. Under normal circumstances
    /// there wouldn't be both a bool that turns the logic off and a float that can scale the
    /// relative impact of the rule to 0. You almost always want the float so you just use it
    /// at 0 to turn off the rule. But we want to be able to toggle the rule on and off
    /// quickly to see the difference it makes.
    ///
    /// This mehtod of attacking will ALWAYS be wrong. By definition, but its a good start. 
    /// More correctly, the enemy should be trying to be at some future location relative 
    /// to the player. Ultimiately we don't want this enemy to annihilate the player so perhaps 
    /// the always wrongness will work in our favour, tweaking is probably easier if the 
    /// algorithm is first tuned to be 'more correct' and then backed off or made less ruthless.
    /// </summary>
    public class EnemyPlaneController : MonoBehaviour
    {
        [SerializeField] protected Weapon weapon;
        [SerializeField] protected PlaneMovement engine;
        [SerializeField] protected Rigidbody2D rb2d;
        [SerializeField] protected Vector2 fireDelayRange = new Vector2(0, 1);
        [SerializeField] protected float distToCheckFacing = 5;
        [SerializeField] protected float distToStartSpeedMatching = 10;
        [SerializeField] protected float minAngleFire = 15;
        [SerializeField] protected float desiredAngleToPlayer = 10;
        [SerializeField] protected float minDistToFire = 15;
        [SerializeField] protected float minThurstWhenMatching = 0.5f;
        [SerializeField] protected float sweetSpotThresholdDot = 0.6f;
        [SerializeField] protected float sweetSpotTurnScale = 0.2f;
        [SerializeField] protected float frontSweetSpotDistance = 3.0f;
        [SerializeField] protected float rearSweetSpotDistance = 1.0f;
        [SerializeField] protected float leadPlayerVelocityScale = 0.0f;
        [SerializeField] protected GameObjectCollectionSO playerCollection;
        [SerializeField] protected float friendlyAheadStartingDistance = 3;
        [SerializeField] protected float friendlyAheadDistanceCap = 10;
        [SerializeField] protected float friendlyAheadRadius = 1;
        [SerializeField] protected float friendlyAheadDistanceSpeedPenalty = 0.5f;
        [SerializeField] protected float friendlyAheadTurnScale = 10;
        [SerializeField] protected LayerMask avoidLayers;

        //ai feature toggles
        [SerializeField]
        protected bool useGeneralSpeedMatching = false,
                       useRandomFireDelay = true,
                       useQuickTurn = true,
                       useSweetSpot = true,
                       useFiringCone = true,
                       useTargetLead = true,
                       useAvoidOverlapSpeedReduce = true,
                       useAvoidOverlapTurnAway = true;

        protected Rigidbody2D playerRB;
        protected float selectedFireDelay, fireDelayCounter;
        protected float prevThrust, prevTorque;
        private const int MaxFriendlyRaycastHits = 10;
        protected RaycastHit2D[] raycastHit2Ds = new RaycastHit2D[MaxFriendlyRaycastHits];

        public Vector3 Facing { get { return transform.up; } }

        private void Start()
        {
            RollFireDelay();
        }

        private void Update()
        {
            if (playerCollection.First == null)
            {
                //TODO do something that makes more sense than engines off
                return;
            }

            if (playerRB == null)
            {
                playerRB = playerCollection.First.GetComponent<Rigidbody2D>();
            }

            fireDelayCounter += Time.deltaTime;

            //assuming we want to go all out but no turn
            float desiredEngineThrust = 1;
            float desiredEngineTorque = 0;

            //Approaching this like a physics problem, gather all data
            var diff = playerRB.transform.position - transform.position;
            var dist = diff.magnitude;
            var diffDir = diff / dist;
            var angleToFacePlayer = Vector3.SignedAngle(Facing, diffDir, Vector3.forward);
            var facingPlayerDot = Vector3.Dot(diffDir, Facing);
            var playerVel = playerRB.velocity;
            var playerFacing = playerRB.transform.up;
            var playerSpeed = playerVel.magnitude;
            var vel = rb2d.velocity;
            var speed = vel.magnitude;
            var targetLeadLocation = playerRB.transform.position + (Vector3)playerRB.velocity * leadPlayerVelocityScale;
            var angleToLeadLoc = angleToFacePlayer;
            bool wantsToSpeedMatchPlayer = false;

            if (useTargetLead)
            {
                angleToLeadLoc = Vector3.SignedAngle(Facing, (targetLeadLocation - transform.position).normalized, Vector3.forward);
            }

            //if we are not facing target then turn
            if (Mathf.Abs(angleToLeadLoc) > desiredAngleToPlayer)
            {
                //turning in shortest direction to player
                desiredEngineTorque = Mathf.Sign(angleToFacePlayer);
            }

            if (useQuickTurn)
            {
                //if too far and facing wrong way
                if (dist > distToCheckFacing)
                {
                    if (facingPlayerDot < 0)
                    {
                        desiredEngineThrust = 0;
                    }

                    RollFireDelay();
                }
            }

            //the speed matching makes them BRUTAL
            if (useGeneralSpeedMatching)
            {
                if (dist < distToStartSpeedMatching)
                {
                    //without the facing check they all become luft ACEs
                    if (facingPlayerDot > 0)
                    {
                        wantsToSpeedMatchPlayer = true;
                    }
                }
            }

            if (useFiringCone)
            {
                //if the player isn the firing cone then try to shoot
                if (Mathf.Abs(angleToFacePlayer) < minAngleFire && dist < minDistToFire)
                {
                    Debug.DrawLine(transform.position, playerRB.transform.position, Color.red);

                    if (fireDelayCounter > selectedFireDelay)
                    {
                        weapon.Fire();
                        RollFireDelay();
                    }
                }
            }
            else
            {
                weapon.Fire();
            }

            if (useSweetSpot)
            {
                //check if we are in the sweet spot for player to fire on or view us, if so act more slowly
                if (Vector3.Dot(-diffDir, playerFacing) > sweetSpotThresholdDot)
                {
                    wantsToSpeedMatchPlayer = true;
                    desiredEngineTorque *= sweetSpotTurnScale;
                }

                //other sweet spot is being front of player and close, we happily stack these
                if (Vector3.Dot(-diffDir, playerFacing) > 0)
                {
                    if (dist < frontSweetSpotDistance)
                    {
                        wantsToSpeedMatchPlayer = true;
                        desiredEngineTorque *= sweetSpotTurnScale;
                    }
                }
                else
                {
                    if (dist < rearSweetSpotDistance)
                    {
                        wantsToSpeedMatchPlayer = true;
                        desiredEngineTorque *= sweetSpotTurnScale;
                    }
                }
            }

            //we don't want to run into a friendly plane
            if (useAvoidOverlapSpeedReduce || useAvoidOverlapTurnAway)
            {
                //is their a friendly right infront of us, if so, slow down a bit

                //many options here, but our AI is pretty simple right now, and whiskers and an easy option here
                //  that should give us decent results without needing to track info between frames.
                //
                //right now this only does 1 large infront of whisker
                var numFriendliesInfrontHit = Physics2D.CircleCastNonAlloc(
                    transform.position + Facing * friendlyAheadStartingDistance,
                    friendlyAheadRadius,
                    Facing,
                    raycastHit2Ds,
                    friendlyAheadDistanceCap,
                    avoidLayers);//, gameObject.layer);

                var closest = SelectClosestHit(numFriendliesInfrontHit);

                if (closest.rigidbody != null && closest.rigidbody != rb2d)
                {
                    if (useAvoidOverlapSpeedReduce)
                    {
                        desiredEngineThrust *= friendlyAheadDistanceSpeedPenalty * (closest.distance / friendlyAheadDistanceCap);
                    }
                    if (useAvoidOverlapTurnAway)
                    {
                        var diffToFriendly = (Vector3)closest.point - transform.position;
                        var angleToLeadingFriendly = Vector3.SignedAngle(Facing, diffToFriendly.normalized, Vector3.forward);
                        //force turn away from angle to friendly
                        //if our and their headings align, then rotate away from their heading quickly as poissible
                        if (Vector3.Dot(Facing, closest.transform.up) > 0.9f)
                        {
                            desiredEngineTorque = -1 * angleToLeadingFriendly * friendlyAheadTurnScale;
                        }
                    }
                    //Debug.DrawRay(transform.position, Facing, Color.red,1);
                    //Debug.DrawLine(transform.position, closest.point, Color.magenta, 1);
                }
            }

            //attempt to speed match
            if (wantsToSpeedMatchPlayer)
            {
                desiredEngineThrust = (playerSpeed - speed) * 0.1f * Time.deltaTime * prevThrust;
                desiredEngineThrust = Mathf.Max(minThurstWhenMatching, desiredEngineThrust);
            }

            engine.SetEngineState(desiredEngineThrust, desiredEngineTorque);

            prevThrust = desiredEngineThrust;
            prevTorque = desiredEngineTorque;
        }

        private RaycastHit2D SelectClosestHit(int numFriendliesInfrontHit)
        {
            RaycastHit2D closest = default;

            if (numFriendliesInfrontHit > 0)
            {
                closest = raycastHit2Ds[0];
                for (int i = 1; i < numFriendliesInfrontHit; i++)
                {
                    if (raycastHit2Ds[i].rigidbody == rb2d)
                        continue;

                    if (closest.rigidbody == rb2d || closest.distance > raycastHit2Ds[i].distance)
                    {
                        closest = raycastHit2Ds[i];
                    }
                }
            }

            return closest;
        }

        protected void RollFireDelay()
        {
            if (useRandomFireDelay)
                selectedFireDelay = Random.Range(fireDelayRange.x, fireDelayRange.y);

            fireDelayCounter = 0;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, rearSweetSpotDistance);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, frontSweetSpotDistance);

            Gizmos.color = Color.cyan;
            var beginAt = transform.position + transform.up * friendlyAheadStartingDistance;
            var endAt = transform.position + transform.up * friendlyAheadDistanceCap;
            Gizmos.DrawWireSphere(beginAt, friendlyAheadRadius);
            Gizmos.DrawWireSphere(endAt, friendlyAheadRadius);
            Gizmos.matrix = Matrix4x4.TRS((beginAt + endAt) / 2, transform.rotation, Vector3.one);
            Gizmos.DrawWireCube(Vector3.zero, new Vector3(friendlyAheadRadius * 2, friendlyAheadDistanceCap - friendlyAheadStartingDistance, 1));
            Gizmos.matrix = Matrix4x4.identity;
        }
    }
}
