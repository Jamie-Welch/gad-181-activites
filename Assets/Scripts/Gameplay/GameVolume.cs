using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameVolume : MonoBehaviour
{
    public AudioSource gameAudio;
    public float voulme;
    public Slider volumeSlider;
    // Start is called before the first frame update
    void Start()
    {
        voulme = PlayerPrefs.GetFloat("AudioVolume");
        volumeSlider.value = voulme;
        gameAudio.volume = voulme;
    }
}
