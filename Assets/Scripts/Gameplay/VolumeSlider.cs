using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    public Slider volumeControl;
    public float currentVolume;
    public PlayerPref prefScript;
    // Start is called before the first frame update
    private void Update()
    {
        volumeControl.value = currentVolume;

        prefScript.SetVolume(currentVolume);
    }
}
