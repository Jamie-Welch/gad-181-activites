using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiatePlayerChoice : MonoBehaviour
{
    protected string weaponChoice;
    [SerializeField] protected GameObject shieldPrefab;
    [SerializeField] protected GameObject lightningPrefab;
    [SerializeField] protected GameObject icePrefab;
    [SerializeField] protected GameObject FirePrefab;
    [SerializeField] protected Transform spawnLocation;


    // Start is called before the first frame update
    void Awake()
    {
        weaponChoice = PlayerPrefs.GetString("WeaponType");

        if (weaponChoice == "Shield")
        {
            Instantiate(shieldPrefab, new Vector3(0, 0, 0), Quaternion.identity, spawnLocation);
        }
        else if (weaponChoice == "Ice")
        {
            Instantiate(icePrefab, new Vector3(0, 0, 0), Quaternion.identity, spawnLocation);
        }
        else if (weaponChoice == "Fireball")
        {
            Instantiate(FirePrefab, new Vector3(0, 0, 0), Quaternion.identity, spawnLocation);
        }
    }

    
    
}
