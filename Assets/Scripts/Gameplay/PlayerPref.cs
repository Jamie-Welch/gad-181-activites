using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPref : MonoBehaviour
{
    public void SetWeapon(string weaponName)
    {
        PlayerPrefs.SetString("WeaponType", weaponName);
    }

    public void SetBody(string hullName)
    {
        PlayerPrefs.SetString("HullType", hullName);
    }

    public void SetWing(string wingName)
    {
        PlayerPrefs.SetString("WingType", wingName);
    }
    public void SetVolume(float volumeAmount)
    {
        PlayerPrefs.SetFloat("AudioVolume", volumeAmount);
    }

}
