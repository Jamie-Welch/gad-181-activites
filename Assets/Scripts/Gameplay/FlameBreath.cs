using SAE;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameBreath : Weapon
{
    [SerializeField] protected float locationOffset;
    [SerializeField] protected Vector3 vectorOffset;
    [SerializeField] protected Vector3 vectorOffsetAmount;
    [SerializeField] protected Transform childTransform;
    protected override void CreateNewProjectile()
    {
        childTransform = GetComponentInChildren<Transform>();
        vectorOffsetAmount.y = locationOffset;
        vectorOffset = childTransform.position + vectorOffsetAmount;
        var newProj = Instantiate(projectilePrefab.gameObject, transform.position, transform.rotation, transform.parent).GetComponent<Projectile>();
        newProj.transform.parent = gameObject.transform;
        newProj.transform.localPosition = new Vector3(0, locationOffset, 0);
        if (projectilesInheritVelocity && rb2D != null)
        {
            newProj.Rigidbody2D.velocity += rb2D.velocity;  
        }
    }
}
