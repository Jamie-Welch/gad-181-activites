using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace SAE
{ 
public class BossSpawner : MonoBehaviour
{ 
    // Start is called before the first frame update
    public Transform bossSpawnLoc;
    public List<GameObject> bossSpawnList = new List<GameObject>();
    public ScoreSystem scoreSystemRef;
    public int bossSpawnScore;

    public void Awake()
    {
        scoreSystemRef = FindObjectOfType<ScoreSystem>();
        scoreSystemRef.bossSpawnThreshold = bossSpawnScore;
    }

    public void SpawnNextBoss()
    {
        if (bossSpawnList.Count > 0)
        {
            Instantiate(bossSpawnList[0], bossSpawnLoc.position, bossSpawnLoc.rotation);
            bossSpawnList.RemoveAt(0);
        }
    }
}
}