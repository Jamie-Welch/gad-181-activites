using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRegen : MonoBehaviour
{
    public PlayerHealth phRef;
    public float regenTimer;
    public float regenAmount;
    // Start is called before the first frame update
    void Start()
    {
        phRef = GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        regenTimer += Time.deltaTime;
        if(regenTimer > 2)
        {
            Debug.Log("Regen Called");
            Invoke("RegenStart", 1f);
        }
    }

    public void RegenStart()
    {
        phRef.AddHP(regenAmount);
    }
}
