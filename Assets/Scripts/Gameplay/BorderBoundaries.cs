using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderBoundaries : MonoBehaviour
{
    public float forceAmount;
    private Rigidbody2D playerRB;
    public void OnTriggerStay2D(Collider2D collision)
    {
        playerRB = collision.GetComponent<Rigidbody2D>();
        if(playerRB.position.y > 7)
        {
            playerRB.AddForce(Vector2.down * forceAmount);
        }
        else
        {
            playerRB.AddForce(Vector2.up * forceAmount);
        }
    }
}
