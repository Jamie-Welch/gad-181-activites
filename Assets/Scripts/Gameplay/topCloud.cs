using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class topCloud : MonoBehaviour
{
    public float forceAmount;
    private Rigidbody2D playerRB;

    // Start is called before the first frame update
    public void OnTriggerStay2D(Collider2D collision)
    {
        playerRB = collision.GetComponent<Rigidbody2D>();
        playerRB.AddForce(Vector2.down * forceAmount);
    }
}
