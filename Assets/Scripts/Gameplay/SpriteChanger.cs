using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour
{
    public string wingType;
    public string hullType;
    public SpriteRenderer sRenderer;
    public Sprite dashHeavy;
    public Sprite dashLight;
    public Sprite dashStand;
    public Sprite gLideHeavy;
    public Sprite glideLight;
    public Sprite glideStand;
    public Sprite standHeavy;
    public Sprite standLight;
    public Sprite standStand;

    // Start is called before the first frame update
    void Start()
    {
        sRenderer = GetComponent<SpriteRenderer>();
        wingType = PlayerPrefs.GetString("WingType");
        hullType = PlayerPrefs.GetString("HullType");
        

        if (wingType == "Dash" && hullType == "Heavy")
        {
            sRenderer.sprite = dashHeavy;
        }

        if (wingType == "Dash" && hullType == "Light")
        {
            sRenderer.sprite = dashLight;
        }

        if (wingType == "Dash" && hullType == "Standard")
        {
            sRenderer.sprite = dashStand;
        }

        if (wingType == "Glide" && hullType == "Heavy")
        {
            sRenderer.sprite = gLideHeavy;
        }

        if (wingType == "Glide" && hullType == "Light")
        {
            sRenderer.sprite = glideLight;
        }

        if (wingType == "Glide" && hullType == "Standard")
        {
            sRenderer.sprite = glideStand ;
        }

        if (wingType == "Standard" && hullType == "Heavy")
        {
            sRenderer.sprite = standHeavy;
        }

        if (wingType == "Standard" && hullType == "Light")
        {
            sRenderer.sprite = standLight;
        }

        if (wingType == "Standard" && hullType == "Standard")
        {
            sRenderer.sprite = standStand;
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
