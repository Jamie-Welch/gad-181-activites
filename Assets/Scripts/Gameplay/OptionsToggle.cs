using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsToggle : MonoBehaviour
{
    public Toggle optionsToggle;


    public void FullScreenToggle()
    {
        if (optionsToggle.isOn == true)
        {
            Screen.fullScreen = true;
        }
        else
        {
            Screen.fullScreen = false;  
        }
    }
}
