using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAE;

public class ColdCone : Weapon
{
    //Variables for rotation
    private Vector3 projectileSpawnOffset = new Vector3();  //the position where we want the cone to hover
    public float offset;    //The amount of offset the cone will be from the player
    //Variables for Mouse Following
    public Transform playerTrans;
    public Vector3 mousePosition;
    public float weaponRadius;
    public bool mouseTargeting;

    public override void Start()
    {
        playerTrans = gameObject.transform;
        base.Start();
    }

    public override void Update()
    {
        mousePosition = Input.mousePosition;
        base.Update();
    }
    protected override void CreateNewProjectile()   //when we fire
    {
        Debug.Log("We fired");
        if (mouseTargeting)
        {
            //Try following Mouse
            mousePosition.z = playerTrans.position.z - Camera.main.transform.position.z;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            Vector3 direction = mousePosition - playerTrans.position;
            direction = Vector3.ClampMagnitude(direction, weaponRadius);

            var coldCone = Instantiate(projectilePrefab.gameObject, playerTrans.position, playerTrans.rotation);
            coldCone.transform.position = playerTrans.position + direction;
        }

        //Functional code for rotating with plane rotation
        else
        {
            projectileSpawnOffset.y = offset;   //the y is the offset amount
            Quaternion newrotation = Quaternion.Euler(0, 180, 0);   //new rotation relative to the players current rotation
            var newProj = Instantiate(projectilePrefab.gameObject, spawnLocation.position + (spawnLocation.up * offset), Quaternion.Inverse(transform.rotation)).GetComponent<Projectile>(); //instantiate the weapon prefab using the offset for location, and rotation

            if (projectilesInheritVelocity && rb2D != null) //this doesn't effect functionality, may not need it
            {
                newProj.Rigidbody2D.velocity += rb2D.velocity;
            }
        }
    }
}
