using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderBoundariesHorizontal : MonoBehaviour
{
    public float forceAmount;
    private Rigidbody2D playerRB;
    public void OnTriggerStay2D(Collider2D collision)
    {
        playerRB = collision.GetComponent<Rigidbody2D>();
        if(playerRB.position.x < -550)
        {
            playerRB.AddForce(Vector2.right * forceAmount);
        }
        else
        {
            playerRB.AddForce(Vector2.left * forceAmount);
        }
    }
}
