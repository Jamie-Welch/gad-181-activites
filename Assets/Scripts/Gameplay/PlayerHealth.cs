using SAE;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
        public float hp;
        public float maxHp;
        public bool isAlive = true;
        public bool isVuln = true;

        public AudioEffectSO deathAudioEffect;
        public GameObject deathEffectPrefab;

        public GameObject boostCanvas;
        public GameObject menuPause;
        public PlayerInputRouter pirScript;
        
        //Jordan Health Redness Mod
        private float redvalue;
        public SpriteRenderer mySpriteRenderer;
        public HealthRegen hrRef;
        //Hull Prefs Mod
        public Hull hullStandard;
        public Hull hullLight;
        public Hull hullHeavy;
        private string hullChoice;



    public virtual void Awake()
        {
        
        boostCanvas = GameObject.Find("CompleteBoost");
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        menuPause = GameObject.Find("PauseMenu");
        pirScript = GetComponent<PlayerInputRouter>();
        menuPause = pirScript.PauseMenu;
        hrRef = GetComponent<HealthRegen>();
                  

        //Hull Health Mod Code
        hullChoice = PlayerPrefs.GetString("HullType");

        //The check of which hull the player game object has attached to it - put in Awake if using.
        if (hullChoice == "Heavy")
        {
            maxHp = maxHp * hullHeavy.hullMaxHPMod;
            hp = maxHp;
            Debug.Log("My Hull is Heavy for Health! and my health is " + maxHp);
        }
        else if (hullChoice == "Light")
        {
            maxHp = maxHp * hullLight.hullMaxHPMod;
            hp = maxHp;
            Debug.Log("My Hull is Light for Health! and my health is " + maxHp);
        }
        else
        {
            maxHp = maxHp * hullStandard.hullMaxHPMod;
            hp = maxHp;
            Debug.Log("My Hull is Standard for Health! and my health is " + maxHp);
        }
        Debug.Log("PlayerHealth after hull calc");
    }

        public virtual void Update()
        {
        //Health Redness
        redvalue = hp / maxHp;
        mySpriteRenderer.color = new Color(1f, redvalue, redvalue, 1f);
        }
        public virtual void ReceiveDamage(float dmg)
        {
            if (!isAlive && !isVuln)
                return;

            hp -= dmg;
            OnDamage();

            if (hp <= 0)
            {
                isAlive = false;
                Death();
            }
        }

        public virtual void AddHP(float amt)
        {
            hp += amt;
            hp = Mathf.Clamp(hp, 0, maxHp);
        }

        protected virtual void Death()
        {
            Destroy(gameObject);
            DoDeathEffects();

            if (gameObject.tag == "Player")
            {
                boostCanvas.gameObject.SetActive(false); //make the boost bar's canvas desabled to hide it from the player
                menuPause.SetActive(true);
            }

        }

        protected virtual void DoDeathEffects()
        {
            if (deathEffectPrefab != null)
            {
                Instantiate(deathEffectPrefab, transform.position, deathEffectPrefab.transform.rotation * transform.rotation);
            }

            if (deathAudioEffect != null)
            {
                deathAudioEffect.Play2D();
            }
        }

        // this is a good starting point, but eventually you might want to pass more context
        protected virtual void OnDamage()
        {
            hrRef.regenTimer = 0;
        }
}
