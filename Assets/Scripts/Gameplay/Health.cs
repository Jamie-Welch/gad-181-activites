﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace SAE
{
    /// <summary>
    /// A Starting point for a Health script, intended to be expanded to meet indivdual games needs, either via
    /// direct modification or inheritence.
    /// </summary>
    public class Health : MonoBehaviour
    {
        [SerializeField] public float hp;
        [SerializeField] public float maxHp;
        [SerializeField] protected bool isAlive = true;
        [SerializeField] protected bool isVuln = true;
        public Button restartButton;

        [SerializeField] protected AudioEffectSO deathAudioEffect;
        [SerializeField] protected GameObject deathEffectPrefab;

        //Jordan Health Mods
        [SerializeField] protected Canvas boostCanvas;  //Reference to the boost canvas so we don't have to worry about passing it in in the inspector.
        private float redValue;                         //The amount of red we want to show 
        private SpriteRenderer mySpriteRenderer;        //Reference to the sprite renderer

        public virtual void Start()
        {
            hp = maxHp;
            //Jordan mods
            boostCanvas = FindObjectOfType<Canvas>();           //Grab the canvas that the boost bar is located on so we don't have to pass it in the inspector
            mySpriteRenderer = GetComponent<SpriteRenderer>();  //Grab the player's sprite renderer to change the colors.
        }
        
        public virtual void Update()
        {
            redValue = hp / maxHp;  //Check the health compared to max health and give a value between 0 and 1
            mySpriteRenderer.color = new Color(1f, redValue,redValue,1f);   //reduce the amount of non-red colours by how much health we have left.
        }

        public virtual void ReceiveDamage(float dmg)
        {
            if (!isAlive && !isVuln)
                return;

            hp -= dmg;
            OnDamage();

            if (hp <= 0)
            {
                isAlive = false;
                Death();
            }
        }

        public virtual void AddHP(float amt)
        {
            hp += amt;
            hp = Mathf.Clamp(hp, 0, maxHp);
        }

        protected virtual void Death()
        {
            Destroy(gameObject);
            DoDeathEffects();
            boostCanvas.gameObject.SetActive(false); //make the boost bar's canvas desabled to hide it from the player
            restartButton.enabled = true;
        }

        protected virtual void DoDeathEffects()
        {
            if (deathEffectPrefab != null)
            {
                Instantiate(deathEffectPrefab, transform.position, deathEffectPrefab.transform.rotation * transform.rotation);
            }

            if (deathAudioEffect != null)
            {
                deathAudioEffect.Play2D();
            }
        }

        // this is a good starting point, but eventually you might want to pass more context
        protected virtual void OnDamage()
        {

        }
    }
}
