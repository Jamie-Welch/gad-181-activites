﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace SAE
{
    /// <summary>
    /// Simple gather of user input, converted to plane movement desired. This separation is intended to
    /// keep the componenets responsible for moving objects clean and free of player centric and/or
    /// input code so they can be resuled by NPCs/Enemies etc.
    /// </summary>
    public class PlayerInputRouter : MonoBehaviour
    {
        [SerializeField] protected PlayerInput playerInput;

        private PlayerInput PlayerController
        {
            get
            {
                if(playerInput == null)
                {
                    playerInput = GetComponent<PlayerInput>();
                    // if we somehow don't get the what we want return null
                    if(playerInput == null)
                    {
                        Debug.LogError("No Player Input Script on " + transform.name);
                        return null;
                    }
                }
                return playerInput;
            }
        }


        [SerializeField] protected PlaneMovement playerMovement;

        private PlaneMovement PlayerMovement
        {
            get
            {
                if(playerMovement == null)
                {
                    playerMovement = GetComponent<PlaneMovement>();
                    if(playerMovement == null)
                    {
                        Debug.LogError("No player movement script on " + transform.name);
                        return null;
                    }
                }
                return playerMovement;
            }
        }

        [SerializeField] protected Weapon weapon;

        private Weapon CurrentWeapon
        {
            get
            {
                if (weapon == null)
                {
                    weapon = GetComponent<Weapon>();
                    if (weapon == null)
                    {
                        Debug.LogError("No weapon script on " + transform.name);
                        return null;
                    }
                }
                return weapon;
            }
        }

        private bool firePressed; // is fire being pressed.
        private Vector2 inputVector; // the rotation.

        private void OnEnable()
        {
            PlayerController.onActionTriggered += ReadControllerInput;
        }

        private void OnDisable()
        {
            PlayerController.onActionTriggered -= ReadControllerInput;
        }

        //Jordan Code
        public BoostBar myBoostBar;     //reference to my boost bar script
        public bool drainNow = false;   //Variable to see if we should re
        public CommonActions caScript; // reference to the Common actions script
        public bool isPaused;
        public GameObject PauseMenu;
        public PauseTime timePause;

        private void Awake()
        {
            myBoostBar = FindObjectOfType<BoostBar>();
            caScript = FindObjectOfType<CommonActions>();
            PauseMenu = GameObject.Find("PauseMenu");
            PauseMenu.SetActive(false);
            timePause = FindObjectOfType<PauseTime>();
        }

        private void Update()
        {
            if(firePressed)
            {
                CurrentWeapon.Fire();
            }
            if (playerInput.actions["Accelerate"].IsPressed())
            {

                Debug.Log("I'm trying to drain boost");
                myBoostBar.DrainBoost();

                if (myBoostBar.currentBoost <= 1f)
                {
                    inputVector.y = 0;
                }
                if(myBoostBar.currentBoost >= myBoostBar.tooTired)
                {
                    inputVector.y = 1;
                    myBoostBar.DrainBoost();
                }
            }
            if (playerInput.actions["Accelerate"].IsPressed() && inputVector.y == 0)
            {
                myBoostBar.RegenBoost();
            }
            else if (!playerInput.actions["Accelerate"].IsPressed())
            {
                myBoostBar.RegenBoost();
            }
            PlayerMovement.SetEngineState(inputVector.y, -inputVector.x);
            
        }


        private void ReadControllerInput(InputAction.CallbackContext context)
        {
            if(context.action.name == "Fire")
            {
                // toggle example
                //if (context.performed)
                //{
                //    firePressed = !firePressed;
                //}

                if (context.performed)
                {
                    firePressed = true;
                }
                else if(context.canceled)
                {
                    firePressed = false;
                }
            }
            else if(context.action.name == "Accelerate") // thurst?
            {
                float amount = context.ReadValue<float>();

                if (context.performed && myBoostBar.tooTired <= myBoostBar.currentBoost)    //If player is trying to accelerate, AND they have enough boost to allow for it
                {
                    drainNow = true;        //When we have the 'go' button pressed, change bool to true so they start draining their boost while moving
                    inputVector.y = amount; //move forward
                }
                else if (context.canceled)
                {
                    drainNow = false;       //When we release to 'go' button is released, bool changed to false and boost starts regenerating  
                    inputVector.y = 0;      //stop moving forward
                }
            }
            else if(context.action.name == "Rotate") // grab left right values
            {
                inputVector.x = context.ReadValue<Vector2>().x;
            }
            else if (context.action.name == "MainMenu") // grab left right values
            {
                isPaused = !isPaused;
                if (isPaused == true) 
                {
                    timePause.Pause();

                    PauseMenu.SetActive(true);
                }

                else
                {
                    timePause.Unpause();
                    PauseMenu.SetActive(false);
                }
                
            }
        }
    }
}
