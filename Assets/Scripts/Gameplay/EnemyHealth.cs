using SAE;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] protected float hp;
    [SerializeField] protected float maxHp;
    [SerializeField] protected bool isAlive = true;
    [SerializeField] protected bool isVuln = true;

    [SerializeField] protected AudioEffectSO deathAudioEffect;
    [SerializeField] protected GameObject deathEffectPrefab;


    //Jordan Score Code
    [SerializeField] protected ScoreSystem scoreSystem; //A reference to the score system so we can call functions and update the score
    [SerializeField] protected int myWorth;             //The number of points this enemy is worth and will pass into the AddScore() function
    //Jordan Health Redness Mod
    private float redvalue;
    private SpriteRenderer mySpriteRenderer;
    private float dmgTimer;
    public float dmgTimerMax;
    public BossSpawner myBossSpawner;

    public virtual void Start()
    {
        
    }

    public virtual void Awake()
    {
        hp = maxHp;
        //Jordan Score Code
        scoreSystem = FindObjectOfType<ScoreSystem>();  //When the enemy spawns, grab a reference to the score system so we can update the score with our value when we die.
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        dmgTimer = dmgTimerMax;
        myBossSpawner = FindObjectOfType<BossSpawner>();
    }

    public virtual void Update()
    {
        //Health Redness
        redvalue = hp / maxHp;
        mySpriteRenderer.color = new Color(1f, redvalue, redvalue, 1f);
        dmgTimer += Time.deltaTime;
        
    }
    public virtual void ReceiveDamage(float dmg)
    {
        if (!isAlive && !isVuln)
            return;

        hp -= dmg;
        OnDamage();

        if (hp <= 0 && dmgTimer >= dmgTimerMax)
        {
            isAlive = false;
            Death();
            dmgTimer = 0;
        }
    }

    public virtual void AddHP(float amt)
    {
        hp += amt;
        hp = Mathf.Clamp(hp, 0, maxHp);
    }

    protected virtual void Death()
    {
        Destroy(gameObject);
        DoDeathEffects();

        //Jordan Score Code
        scoreSystem.AddScore(myWorth);  //Call the AddScore function to add this enemies value to the players score, multiplying if necessary
        scoreSystem.TimerReset();       //Reset the timer when we get a kill so player has time to get more and stack multiplier
    }

    protected virtual void DoDeathEffects()
    {
        if (deathEffectPrefab != null)
        {
            Instantiate(deathEffectPrefab, transform.position, deathEffectPrefab.transform.rotation * transform.rotation);
        }

        if (deathAudioEffect != null)
        {
            deathAudioEffect.Play2D();
        }
    }

    // this is a good starting point, but eventually you might want to pass more context
    protected virtual void OnDamage()
    {
    }
}
