using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SAE
{

    public class PlayerPlaneMovement : PlaneMovement
    {
        public string wingChoice;
        public Wing wingGlide;
        public Wing wingDash;
        public Wing wingStandard;

        public void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            wingChoice = PlayerPrefs.GetString("WingType");

            //The check of which hull the player game object has attached to it - put in Awake if using.
            if (wingChoice == "Dash")
            {
                engineForce = engineForce * wingDash.wingEngineForceMod;
                Debug.Log("My wing is Dash for Engine Force and Turn Speed!");
            }
            else if (wingChoice == "Glide")
            {
                engineForce = engineForce * wingGlide.wingEngineForceMod;
                Debug.Log("My wing is Glide for Engine Force and Turn Speed!");
            }
            else
            {
                engineForce = engineForce * wingStandard.wingEngineForceMod;
                Debug.Log("My wing is Standard for Engine Force and Turn Speed!");
            }
        }
    }
}