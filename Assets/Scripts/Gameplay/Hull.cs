using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SAE;

[CreateAssetMenu(fileName = "Hull", menuName = "ScriptableObjects/Hull", order = 1)]

public class Hull : ScriptableObject
{

    //This script works just to house some variables that will be accessed from relevant scripts i.e. The player drag coeff script will take the CoeffFricMod value
    public float hullMaxHPMod;
    public float hullHPRegenMod;
    public float hullMaxBoostMod;
    public float hullCoeffFricMod;
}

