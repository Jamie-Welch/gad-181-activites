﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SAE
{
    /// <summary>
    /// Transition between scenes with a loading screen that can fade in and out.
    /// Intended to live on the bootstrap.
    /// </summary>
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] protected float TFBTime = 0.5f;
        [SerializeField] protected LeanTweenType tweenType = LeanTweenType.easeInOutQuart;
        [SerializeField] protected CanvasGroup rootPanel;

        protected static LoadingScreen inst;

        private void Awake()
        {
            inst = this;
        }

        private void Start()
        {
            rootPanel.alpha = 0;
        }

        static public void LoadScene(string name)
        {
            if (inst == null)
            {
                Debug.LogError("No loading screen present");
                return;
            }

            LeanTween.alphaCanvas(inst.rootPanel, 1, inst.TFBTime).setEase(inst.tweenType);
            inst.StartCoroutine(LoadingProgress(name));
        }

        static protected IEnumerator LoadingProgress(string name)
        {
            if (inst == null)
            {
                Debug.LogError("No loading screen present");
                yield break;
            }

            var curScene = SceneManager.GetActiveScene();
            yield return SceneManager.LoadSceneAsync(name);
            LeanTween.alphaCanvas(inst.rootPanel, 0, inst.TFBTime).setEase(inst.tweenType);
        }
    }
}
