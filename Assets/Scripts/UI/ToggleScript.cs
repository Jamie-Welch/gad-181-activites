using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScript : MonoBehaviour
{
    public Toggle lightningToggle;
    public Toggle shieldToggle;
    public Toggle iceToggle;
    public Toggle fireToggle;

    public PlayerPref prefScript;
    // Start is called before the first frame update
    void Update()
    {
        if (lightningToggle.isOn)
        {
            prefScript.SetWeapon("Lightning");
            Debug.Log("YOu have selected Lightning!");
        }

        else if (shieldToggle.isOn)
        {
            prefScript.SetWeapon("Shield");
            Debug.Log("You have selected Shield");
        }
        else if (iceToggle.isOn)
        {
            prefScript.SetWeapon("Ice");
            Debug.Log("You have selected Ice");
        }
        else if (fireToggle.isOn)
        {
            prefScript.SetWeapon("Fireball");
            Debug.Log("You have selected Fireball");
        }


    }

  
}
