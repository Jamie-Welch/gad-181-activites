﻿using JetBrains.Annotations;
using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Simple utility class to cause music to play when it enables. Intended to be used to trigger
    /// music changes based on scene changes or enabling and disabling UI elements.
    /// </summary>
    public class PlayMusic : MonoBehaviour
    {
        public AudioClip clip;

        public void OnEnable()
        {
            FindObjectOfType<MusicManager>().PlayClip(clip);
        }
    }
}
