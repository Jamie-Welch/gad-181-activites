using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using System;
using UnityEngine;

public class MainMenuMusic : MonoBehaviour
{
    public AudioSource audioSource;
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();


    }

    // Update is called once per frame
    void Update()
    {
        DontDestroyOnLoad(gameObject);  
    }
}
