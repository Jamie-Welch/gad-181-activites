﻿using UnityEngine;
using UnityEngine.Audio;

namespace SAE
{
    /// <summary>
    /// A asset that defines a set of sfx to be played, allowing for configuration of more variable audioclips
    /// without having to hide or reimplement in each location that needs it.
    /// </summary>
    [CreateAssetMenu()]
    public class AudioEffectSO : ScriptableObject
    {
        [SerializeField] protected AudioClip[] clips;

        [MinMax(0, 1)]
        [SerializeField] protected Vector2 volumeRange = new Vector2(1, 1);

        [MinMax(-3, 3)]
        [SerializeField] protected Vector2 pitchRange = new Vector2(1, 1);

        [SerializeField] protected AudioMixerGroup group;

        public void Play(AudioSource source, bool oneShot = true)
        {
            if (source == null || clips == null || clips.Length == 0)
                return; //bail out not possible

            var clip = clips[Random.Range(0, clips.Length)];
            var curVol = Random.Range(volumeRange.x, volumeRange.y);
            var curPitch = Random.Range(pitchRange.x, pitchRange.y);
            source.outputAudioMixerGroup = group;

            if (oneShot)
            {
                source.PlayOneShot(clip);
            }
            else
            {
                source.clip = clip;
                source.pitch = curPitch;
                source.volume = curVol;
                source.Play();
            }
        }

        public void Play2D()
        {
            var s = AudioSourcePool.GetSource();
            s.spatialBlend = 0;
            Play(s, false);
            AudioSourcePool.ReturnSourceWhenDone(s);
        }

        public void Play2D(float fixedPan)
        {
            var s = AudioSourcePool.GetSource();
            s.spatialBlend = 0;
            s.panStereo = fixedPan;
            Play(s, false);
            AudioSourcePool.ReturnSourceWhenDone(s);
        }

        public void Play3D(Vector3 pos, AudioRolloffMode audioRolloffMode = AudioRolloffMode.Logarithmic)
        {
            var s = AudioSourcePool.GetSource();
            s.spatialBlend = 1;
            s.transform.position = pos;
            s.rolloffMode = audioRolloffMode;
            Play(s, false);
            AudioSourcePool.ReturnSourceWhenDone(s);
        }
    }
}
