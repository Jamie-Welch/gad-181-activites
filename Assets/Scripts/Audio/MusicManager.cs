﻿using UnityEngine;
using UnityEngine.Audio;

namespace SAE
{
    /// <summary>
    /// Maintains a single audio source for playback of a clip.
    /// 
    /// Intended to live on the Bootstrap.
    /// </summary>
    public class MusicManager : MonoBehaviour
    {
        [SerializeField] protected AudioMixerGroup musicGroup;
        [SerializeField] protected float fadeOutTime = 0.15f;
        protected AudioSource lastAudioSource;

        public void PlayClip(AudioClip clip, bool restartIfSame = false)
        {
            if (restartIfSame || lastAudioSource == null || lastAudioSource.clip != clip)
            {
                var fadeOutSource = lastAudioSource;

                if (fadeOutSource != null)
                {
                    LeanTween.value(fadeOutSource.volume, 0, fadeOutTime)
                        .setOnUpdate((v) => fadeOutSource.volume = v)
                        .setOnComplete(() => Destroy(fadeOutSource));
                }

                //a customisable fade in might be a nice idea here
                lastAudioSource = gameObject.AddComponent<AudioSource>();
                lastAudioSource.playOnAwake = false;
                lastAudioSource.spatialize = false;
                lastAudioSource.loop = true;
                lastAudioSource.clip = clip;
                lastAudioSource.outputAudioMixerGroup = musicGroup;
                lastAudioSource.Play();
            }
        }
    }
}
