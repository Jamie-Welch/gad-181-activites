﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// MonoBehaviour that handles adding and removing to a CollectionSO based on enable and disable.
    ///
    /// See GameObjectCollectionBeh as an example
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U">Concrete class that is CollectionSO<T>, required so we can show it in the inspector</typeparam>
    public abstract class CollectionSOBeh<TStoredObject, TStoredObjectCollection> : MonoBehaviour 
        where TStoredObjectCollection : CollectionSO<TStoredObject>
    {
        [Tooltip("Collection to place in. An object can be in many.")]
        public TStoredObjectCollection[] collections;

        /// <summary>
        /// Required to know how to get that object into the collection.
        ///
        /// For GameObject, this would simply be return gameObject;
        /// </summary>
        /// <returns></returns>
        public abstract TStoredObject GetObjectForCollection();

        private void OnEnable()
        {
            for (int i = 0; i < collections.Length; i++)
            {
                collections[i].Add(GetObjectForCollection());
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < collections.Length; i++)
            {
                collections[i].Remove(GetObjectForCollection());
            }
        }
    }
}
