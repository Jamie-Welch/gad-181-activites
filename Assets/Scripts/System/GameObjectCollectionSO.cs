﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Specialised collection for housing gameobject references.
    /// </summary>
    [CreateAssetMenu(menuName = "GameObject Collection")]
    public class GameObjectCollectionSO : CollectionSO<GameObject>
    {
    }
}
