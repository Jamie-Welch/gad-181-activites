﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Forces Unity to load the BootstrapPrefab when entering play mode or when starting the game.
    /// </summary>
    static public class BootstrapLoader
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void DoBootstrap()
        {
            Bootstrap.Instance.Init();
        }
    }

    [AddComponentMenu("")]
    /// <summary>
    /// Singleton for addition of core bootstrap actions. You may leave this as is and add more components manually
    /// to the BootstrapPrefab. Add new components to that BootstrapPrefab in your Resources folder to add more
    /// functionality to the core bootstrap.
    ///
    /// Using Awake on components added to the prefab should enable you to ensure execution order.
    /// </summary>
    public class Bootstrap : MonoBehaviour
    {
        public const string BootstrapPrefab = "BootstrapCore";
        protected static Bootstrap inst;

        public static Bootstrap Instance
        {
            get
            {
                if (inst == null)
                {
                    //Bootstrap prefab needs to be in a 'Resources' folder for this to work
                    var bootstrapPre = Resources.Load(BootstrapPrefab, typeof(GameObject));
                    if (bootstrapPre == null)
                    {
                        Debug.LogError("Cound not find " + BootstrapPrefab + " within Resources.");
                    }
                    else
                    {
                        inst = GameObject.Instantiate(bootstrapPre as GameObject).GetComponent<Bootstrap>();

                        if (inst == null)
                        {
                            Debug.LogError(BootstrapPrefab + " was found, but does not have a Bootstrap Component.");
                        }
                    }
                }
                return inst;
            }
        }

        public void Init()
        {
            //You can insert additional manuall calls here should you need to, the DoBoostrap Calls this function.
        }
    }
}
