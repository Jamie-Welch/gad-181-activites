using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace SAE
{
    public class ScoreChildScript : MonoBehaviour
    {
        //variables
        [SerializeField] public ScoreSystem parentScoreSystem;
        [SerializeField] public Text myTextComponent;

        private void Awake()
        {
            parentScoreSystem = FindObjectOfType<ScoreSystem>();
            myTextComponent = GetComponent<Text>();
        }
        void Update()
        {
            myTextComponent.text = parentScoreSystem.scoreString + parentScoreSystem.currentScore;
        }
    }
}