﻿using UnityEngine;

namespace SAE.Example
{
    /// <summary>
    /// This goes on the object to be collected as a powerup, the logic for execution of
    /// powerups lives elsewhere.
    /// </summary>
    public class PowerUpBehaviour : MonoBehaviour
    {
        public enum PowerUpType
        {
            Missiles,
            Bomb,
            Plasma,
            //Add more here to add more 'types' of powerup
        }

        //type, or what data
        [SerializeField] protected PowerUpType powerUpType;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //only things that move can get powerups
            if (collision.attachedRigidbody != null &&
                collision.attachedRigidbody.CompareTag("Player"))
            {
                var puc = collision.attachedRigidbody.GetComponent<PowerUpController>();
                puc.ReceivePowerUp(powerUpType);

                //effects
                Destroy(gameObject);
            }
        }
    }
}
