﻿using UnityEngine;
using UnityEngine.Assertions;

namespace SAE.Example
{
    //or this could be SetPartsFromConfigs and do ALL OF IT
    /// <summary>
    /// Assigns parts from SOs into the instance in the scene. Most likely from the
    /// store of selected parts, which might be static or on the bootstrap or kept by name
    /// onto the player prefab in the scene.
    /// </summary>
    public class SetEngineFromConfig : MonoBehaviour
    {
        [SerializeField] protected EngineConfigSO engineConfig;
        [SerializeField] protected GameObjectCollectionSO playerCol;
        [SerializeField] protected GameObject root;

        public void SetNow()
        {
            var playerGO = playerCol.First;
            var planeMovement = playerGO.GetComponent<PlaneMovement>();
            Assert.IsNotNull(playerGO, "Cannot set parts on a player when there is no player found.");
            Assert.IsNotNull(engineConfig, "Cannot set parts on player when there is no EngineConfig.");
            Assert.IsNotNull(planeMovement, "Cannot configure engine when there is no PlaneMovement on the player GO");

            planeMovement.EngineForce = engineConfig.force;
            planeMovement.RotateSpeed = engineConfig.turnRate;

            var engineSFX = playerGO.GetComponent<EngineSFX>();
            Assert.IsNotNull(engineSFX, "Cannot configure engine when there is no EngineSFX on the player GO");
            engineSFX.SetAudioClip(engineConfig.engineSFXClip);
            engineSFX.CopyFrom(engineConfig.engineSFXDroneSettings);

            var engineVis = playerGO.GetComponent<EngineVisualEffects>();
            Assert.IsNotNull(engineSFX, "Cannot configure engine when there is no EngineVisualEffects on the player GO");
            engineVis.SetVisualSprite(engineConfig.sprite, engineConfig.tint);

            if(root != null)
            {
                root.SetActive(false);
            }
        }
    }
}
