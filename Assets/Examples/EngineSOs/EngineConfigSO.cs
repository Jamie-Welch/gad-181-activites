﻿using UnityEngine;

namespace SAE.Example
{
    /// <summary>
    /// Example container for information that can be customised about the engine.
    /// By creating many of these and then keeping track of the 'selected' one, the
    /// we can make many very quickly and test by dragging and droppping, while menus
    /// and load and save are still being worked on.
    /// </summary>
    [CreateAssetMenu()]
    public class EngineConfigSO : ScriptableObject
    {
        public float force = 50, turnRate = 300;

        public ValueMappedAudioDrone engineSFXDroneSettings;
        public AudioClip engineSFXClip;
        public Sprite sprite;
        public Color tint;
    }
}
