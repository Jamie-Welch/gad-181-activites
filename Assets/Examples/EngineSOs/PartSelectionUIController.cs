﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SAE.Example
{
    public class PartSelectionUIController : MonoBehaviour
    {
        // Start is called before the first frame update
        private void OnEnable()
        {
            Time.timeScale = 0;
        }

        private void OnDisable()
        {
            Time.timeScale = 1;
        }
    }
}