﻿using UnityEngine;

namespace SAE.Example
{
    public class EngineVisualEffects : MonoBehaviour
    {
        [SerializeField] protected PlaneMovement planeMovement;
        [SerializeField] protected SpriteRenderer engineSpriteRenderer;

        public void SetVisualSprite(Sprite sprite, Color tint)
        {
            engineSpriteRenderer.sprite = sprite;
            engineSpriteRenderer.color = tint;
        }
    }
}
